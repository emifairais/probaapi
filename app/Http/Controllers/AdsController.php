<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;
use DateTime;

class AdsController extends Controller
{
    public function index()
    {
        $ads = Ad::all();
        foreach($ads as $ad) {
            $images = Image::where('ad_id', $ad->id)->get();
            $images_array = [];
            $string = $ad->created_at;
            $object = new DateTime($string);
            $ad->date = $object->format('d M');
            unset($ad->created_at);
            unset($ad->updated_at);
            foreach($images as $image) {
                array_push($images_array, $image->file_path);
            }
            $ad->images = $images_array;
        }
        return response($ads);
    }

    public function add(Request $request)
    {
        if (!$request->file('file')) {
            return response([
                'code' => 'image.error',
                'title' => 'Eroare încărcare imagini',
                'message' => 'Imaginile nu au fost găsite sau formatul lor nu este acceptat!',
            ]);
        }

        $ad = Ad::create([
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
            'currency' => $request->currency,
            'county' => $request->county,
        ]);

        if ($ad) {
            $response = [
                'code' => 'ad.uploaded',
                'title' => 'Încarcare anunț',
                'message' => 'Anunțul a fost încărcat cu success!',
            ];

            foreach ($request->file('file') as $file) {
                $path = Storage::disk('public')->put('images', $file);
                $data_file = [
                    'file_path'     => $path,
                    'ad_id'         => $ad->id,
                ];
                $image = Image::create($data_file);
                if (!$image) {
                    $response = [
                        'code' => 'image.partial',
                        'title' => 'Eroare încărcare imagini',
                        'message' => 'Una sau mai multe dintre imagini nu a fost acceptată. Vă rugăm încercați din nou!',
                    ];
                }
            }

            return response($response);
        } else {
            return response([
                'code' => 'ad.error',
                'title' => 'Încarcare anunț',
                'message' => 'Anunțul nu a putut fi creat în acest moment. Vă rugăm încercați din nou mai târziu!',
            ]);
        }
    }
}
